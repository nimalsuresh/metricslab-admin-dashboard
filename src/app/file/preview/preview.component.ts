import { AuthService } from './../../service/auth.service';
import { Router } from '@angular/router';
import { FileService } from './../../service/file.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {
  fileList: any[]=[];

  constructor(private fileService:FileService,private route:Router,private authservice:AuthService) { }

  ngOnInit(): void {
    this.getFiles()
  }


  getFiles(){
    this.fileService.getFiles().subscribe(res => {
      console.log(res)
      this.fileList = res['data']
    })
  }

  download(iFile){
console.log(iFile)
   let base64String = iFile['file']
// console.log(base64String)
   let fileName = iFile['name']

   this.downloadPdf(base64String, fileName)
  }

  downloadPdf(base64String, fileName) {
    const source = base64String;
    console.log(source)
    const link = document.createElement("a");
    link.href = source;
    link.download = `${fileName}.pdf`
    link.click();
  }

  back(){
    this.route.navigateByUrl('/file/upload')
  }

  logout(){
    this.authservice.logout()
  }

}
