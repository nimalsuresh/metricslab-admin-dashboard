import { Router } from '@angular/router';
import { User } from './../../user';
import { AuthService } from './../../service/auth.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
email= "admin@admin.com";
pwd ="admin12345";
user:User;
emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
  constructor(private authservice: AuthService, private route: Router) { }

  ngOnInit(): void {
    // this.authservice.login()
  }

  loginSubmit(loginData){
console.log(loginData)
    let userData = {
      email : this.email,
      password: this.pwd
    }
    console.log(userData)

    this.authservice.login(userData).subscribe(res=> {
      console.log(res)
      if(res['token']){
        alert('login successfully')
        this.route.navigateByUrl('/file/preview')
      }else{
        alert("invalid user")
      }

    },error => alert(error.error.message))
  }


}
