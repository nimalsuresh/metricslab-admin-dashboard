import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  API_URL:string = environment.baseUrl;
  headers = new HttpHeaders().set('Content-Type', 'application/json');
  token = JSON.parse(localStorage.getItem('user'))
  
  constructor(private httpClient: HttpClient,public router: Router) { }

  getFiles(){
    let header = new HttpHeaders().set(
      "x-access-token",
      this.token
    );
    let fileviewURL = this.API_URL + '/admin/list-all-user-files' ;
    return this.httpClient.get(fileviewURL, {headers : header})
  }
}
