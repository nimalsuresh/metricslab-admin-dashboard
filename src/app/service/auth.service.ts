import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { BehaviorSubject,Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User } from '../user';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  API_URL:string = environment.baseUrl;
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;

  constructor(private httpClient: HttpClient,public router: Router) 
  {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
    this.user = this.userSubject.asObservable();
   }
  

  //LOGIN_ADMIN
  login(userData) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    let loginURL = this.API_URL + '/admin/login' ;
    
    return this.httpClient.post<any>(loginURL ,userData, { headers:headers })
    .pipe(map(user => {
      localStorage.setItem('user', JSON.stringify(user.token));
      this.userSubject.next(user);
      return user;
    }))
  }

  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/login']);
}

  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      msg = error.error.message;
    } else {
      // server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }
}
